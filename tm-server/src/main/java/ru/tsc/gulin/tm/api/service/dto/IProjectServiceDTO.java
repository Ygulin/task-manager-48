package ru.tsc.gulin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;
import ru.tsc.gulin.tm.enumerated.Status;

import java.util.Date;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {

    void changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
