package ru.tsc.gulin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.gulin.tm.dto.model.AbstractModelDTO;
import ru.tsc.gulin.tm.enumerated.Sort;

import java.util.List;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort);

    void removeById(@Nullable String id);


}
