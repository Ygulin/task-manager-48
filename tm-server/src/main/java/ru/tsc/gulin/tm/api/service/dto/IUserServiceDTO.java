package ru.tsc.gulin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.dto.model.UserDTO;

public interface IUserServiceDTO extends IServiceDTO<UserDTO> {

    @NotNull
    UserDTO create(@Nullable final String login, @Nullable final String password);

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    );

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    );

    @Nullable
    UserDTO findByLogin(@Nullable final String login);

    @Nullable
    UserDTO findByEmail(@Nullable final String email);

    Boolean isLoginExists(@Nullable final String login);

    Boolean isEmailExists(@Nullable final String email);

    void lockUserByLogin(@Nullable final String login);

    void removeByLogin(@Nullable final String login);

    void setPassword(@Nullable final String id, @Nullable final String password);

    void unlockUserByLogin(@Nullable final String login);

    void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    );

}
