package ru.tsc.gulin.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.IRepository;
import ru.tsc.gulin.tm.model.AbstractModel;
import ru.tsc.gulin.tm.enumerated.Sort;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort);

    void removeById(@Nullable String id);

}
