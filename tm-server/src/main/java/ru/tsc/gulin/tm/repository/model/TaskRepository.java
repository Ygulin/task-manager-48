package ru.tsc.gulin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.ITaskRepository;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task task = new Task(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task(name);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
