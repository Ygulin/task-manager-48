package ru.tsc.gulin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.gulin.tm.model.AbstractUserOwnedModel;
import ru.tsc.gulin.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    void removeById(@Nullable String userId, @Nullable String id);

}
