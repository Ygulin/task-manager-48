package ru.tsc.gulin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.gulin.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.gulin.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepositoryDTO<M>, IServiceDTO<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    void removeById(@Nullable String userId, @Nullable String id);

}
