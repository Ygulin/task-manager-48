package ru.tsc.gulin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.gulin.tm.dto.model.TaskDTO;
import ru.tsc.gulin.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gulin.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gulin.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.gulin.tm.exception.field.TaskIdEmptyException;
import ru.tsc.gulin.tm.exception.field.UserIdEmptyException;


import java.util.List;
import java.util.Optional;

public class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    private final IProjectServiceDTO projectService;

    @NotNull
    private final ITaskServiceDTO taskService;

    public ProjectTaskServiceDTO(
            @NotNull final IProjectServiceDTO projectService,
            @NotNull final ITaskServiceDTO taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskService.updateProjectIdById(userId, taskId, projectId);
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks != null) {
            tasks.forEach(m -> {
                try {
                    taskService.removeById(userId, m.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        projectService.removeById(userId, projectId);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskService.updateProjectIdById(userId, taskId, null);
    }

}
