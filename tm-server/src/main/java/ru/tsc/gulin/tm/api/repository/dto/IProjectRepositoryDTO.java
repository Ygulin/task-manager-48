package ru.tsc.gulin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

    @NotNull
    ProjectDTO create(@NotNull String userId,
                      @NotNull String name,
                      @NotNull String description);

    @NotNull
    ProjectDTO create(@NotNull String userId,
                      @NotNull String name);

}
