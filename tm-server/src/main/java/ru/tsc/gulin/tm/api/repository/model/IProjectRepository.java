package ru.tsc.gulin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name
    );

}
