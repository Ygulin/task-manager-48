package ru.tsc.gulin.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.gulin.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IPropertyService;
import ru.tsc.gulin.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.gulin.tm.comparator.NameComparator;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;
import ru.tsc.gulin.tm.dto.model.UserDTO;
import ru.tsc.gulin.tm.marker.UnitCategory;
import ru.tsc.gulin.tm.migration.AbstractSchemeTest;
import ru.tsc.gulin.tm.repository.dto.ProjectRepositoryDTO;
import ru.tsc.gulin.tm.service.ConnectionService;
import ru.tsc.gulin.tm.service.PropertyService;
import ru.tsc.gulin.tm.service.dto.ProjectServiceDTO;
import ru.tsc.gulin.tm.service.dto.TaskServiceDTO;
import ru.tsc.gulin.tm.service.dto.UserServiceDTO;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.tsc.gulin.tm.constant.ProjectTestData.*;
import static ru.tsc.gulin.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.gulin.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectRepositoryDTOTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private static final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private static final IUserServiceDTO userService = new UserServiceDTO(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static IProjectRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepositoryDTO(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);

        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() {
        @Nullable final UserDTO user = userService.findByLogin(USER_TEST_LOGIN);
        if (user != null) userService.remove(user);
        connectionService.close();
    }

    @Before
    public void initTest() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, USER_PROJECT1);
            repository.add(userId, USER_PROJECT2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clean() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(userId, USER_PROJECT3));
            entityManager.getTransaction().commit();
            @Nullable final ProjectDTO project = repository.findOneById(userId, USER_PROJECT3.getId());
            Assert.assertNotNull(project);
            Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
            Assert.assertEquals(userId, project.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTO project = repository.create(userId, USER_PROJECT3.getName());
            Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
            Assert.assertEquals(userId, project.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserIdWithDescription() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final ProjectDTO project = repository.create(userId, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
            Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
            Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
            Assert.assertEquals(userId, project.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<ProjectDTO> projects = repository.findAll(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        entityManager.close();
    }

    @Test
    public void findAllComparatorByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<ProjectDTO> projects = repository.findAll(userId, comparator);
        Assert.assertNotNull(projects);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        entityManager.close();
    }

    @Test
    public void existsByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(userId, USER_PROJECT1.getId()));
        entityManager.close();
    }

    @Test
    public void findOneByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = repository.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
        entityManager.close();
    }

    @Test
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, repository.getSize(userId));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(userId, USER_PROJECT2);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_PROJECT2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void getSizeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(userId));
        entityManager.close();
    }

    @Test
    public void update() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            USER_PROJECT1.setName(USER_PROJECT3.getName());
            repository.update(USER_PROJECT1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_PROJECT3.getName(), repository.findOneById(userId, USER_PROJECT1.getId()).getName());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
