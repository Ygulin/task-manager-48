package ru.tsc.gulin.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.gulin.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IPropertyService;
import ru.tsc.gulin.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.gulin.tm.dto.model.UserDTO;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.marker.UnitCategory;
import ru.tsc.gulin.tm.migration.AbstractSchemeTest;
import ru.tsc.gulin.tm.repository.dto.UserRepositoryDTO;
import ru.tsc.gulin.tm.service.ConnectionService;
import ru.tsc.gulin.tm.service.PropertyService;
import ru.tsc.gulin.tm.service.dto.ProjectServiceDTO;
import ru.tsc.gulin.tm.service.dto.TaskServiceDTO;
import ru.tsc.gulin.tm.service.dto.UserServiceDTO;
import ru.tsc.gulin.tm.util.HashUtil;

import javax.persistence.EntityManager;

import static ru.tsc.gulin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryDTOTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private static final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private static final IUserServiceDTO userService = new UserServiceDTO(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static IUserRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepositoryDTO(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);

        @NotNull final UserDTO user = userService.add(USER_TEST);
    }

    @AfterClass
    public static void tearDown() {
        @Nullable final UserDTO user = userService.findByLogin(USER_TEST_LOGIN);
        if (user != null) userService.remove(user);
    }

    @After
    public void clean() {
        @Nullable UserDTO user = userService.findOneById(ADMIN_TEST.getId());
        if (user != null) userService.remove(user);
        connectionService.close();
    }

    @Test
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(ADMIN_TEST));
            entityManager.getTransaction().commit();
            @Nullable final UserDTO user = repository.findOneById(ADMIN_TEST.getId());
            Assert.assertNotNull(user);
            Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
        entityManager.close();
    }

    @Test
    public void findOneById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void removeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(ADMIN_TEST);
            Assert.assertNotNull(repository.findOneById(ADMIN_TEST.getId()));
            repository.remove(ADMIN_TEST);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void create() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD));
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createWithEmail() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), ADMIN_TEST_EMAIL);
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createWithRole() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), Role.ADMIN);
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            Assert.assertEquals(Role.ADMIN, user.getRole());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findByLogin() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @Nullable final UserDTO user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void findByEmail() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @Nullable final UserDTO user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void isLoginExists() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        Assert.assertTrue(repository.isLoginExists(USER_TEST.getLogin()));
        entityManager.close();
    }

    @Test
    public void isEmailExists() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        Assert.assertTrue(repository.isEmailExists(USER_TEST.getEmail()));
        entityManager.close();
    }

}
